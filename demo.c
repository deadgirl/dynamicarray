#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "src/dynamicarray.c"

void
handleErrors(const dynamicarray *array) {
    if(!array->err)
        return;

    switch(array->err) {
        case ERR_INDEX:
            fprintf(stderr, "Invalid index provided\n");
            break;
        case ERR_MEMORY:
            fprintf(stderr, "Memory allocation failed\n");
            break;
        case ERR_OVERFLOW:
            fprintf(stderr, "Too many entries\n");
            break;
    }

    exit(EXIT_FAILURE);
}

void
printHistory(const dynamicarray *history) {
    printf("\nHistory: {\n");
    for(size_t i=0; i<history->size; i++)
        printf("\t%s\n", (char *)history->get[i]);
    printf("}\n\n");
}

void
appendHistory(dynamicarray *destination, const char *source) {
    dynamicarray_add(
        destination,
        calloc(
            strlen(source),
            sizeof(char)
        )
    );
    handleErrors(destination);
    if(!destination->get[destination->size-1]) {
        fprintf(stderr, "Calloc failed to allocate buffer: (%s)\n", source);
        exit(EXIT_FAILURE);
    }

    strcpy((char *)destination->get[destination->size-1], source);
}

int
main(void) {
    dynamicarray array = dynamicarray_init();
    char buffer[1024];

    while(1) {
        memset(buffer, 0, sizeof(buffer));
        printf("> ");
        scanf(" %1023[^\n]", buffer);

        if(strcmp(buffer, "exit") == EXIT_SUCCESS || strlen(buffer) == 0)
            break;
        else if(strcmp(buffer, "history") == EXIT_SUCCESS) {
            printHistory(&array);
            continue;
        }

        appendHistory(&array, buffer);
    }
    printf("\n");
}