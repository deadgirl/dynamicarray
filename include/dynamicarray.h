/**
* Name: dynamicarray
* Written by Madison Lynch <madi@mxdi.xyz>
* Source: https://gitlab.com/deadgirl/dynamicarray
* License: MIT (See LICENSE file for copyright and license details)
*
* Overview:
* Dynamicarray is a C library that implements type-independent, dynamically
* sized arrays. Entries are stored as "size_t"
* (32-bit: 4bytes, 64-bit: 8bytes), accommodating for all standard types and
* pointers. Make sure to cast when accessing items.
*
* Compilation Instructions:
* If installed, you can include dynamicarray in your project by adding
* "#include <dynamicarray.h>" to your project, then compile with the
* "-ldynamicarray" flag.
* Ex: gcc main.c -ldynamicarray
*
* If compiling locally, add "#include "path/to/dynamicarray.c"" to your project.
* Ex: gcc main.c
*
* Initialization Example:
*   dynamicarray array = dynamicarray_init();
*
* dynamicarray Functions:
* When using the dynamicarray functions, pass the dynamicarray by reference.
*   dynamicarray dynamicarray_init(void);
*   void dynamicarray_add(dynamicarray *array, ...);
*   void dynamicarray_insert(dynamicarray *array, const size_t index, ...);
*   size_t dynamicarray_remove(dynamicarray *array, const size_t index);
*   void dynamicarray_clear(dynamicarray *array);
*
* Accessing:
* Since entries are stored as "size_t" (to preserve type-independence),
* make sure to cast when accessing them (no need to do this for storing).
* Ex:
*   dynamicarray_add(&array, 13);
*   dynamicarray_add(&array, "thirteen");
*   printf("%d\n", (int)array.get[0]);
*   printf("%s\n", (char*)array.get[1]);
*
* Error Handling:
* The provided functions may identify an issue and return early. To ensure
* safety, you can check the "err" member and handle accordingly.
* Errors:
*   0 (ERR_NONE):     No errors
*   1 (ERR_INDEX):    Invalid index provided
*   2 (ERR_MEMORY):   Memory allocation failed (realloc())
*   3 (ERR_OVERFLOW): Addition of another entry would cause an integer overflow
* Ex:
*   dynamicarray_remove(&array, 5); // Invalid index
*   if(array.err) { // Only evaluates as true if there's an error
*     switch(array.err) {
*       case ERR_INDEX:
*         ... // This code would execute
*         break;
*       case ERR_MEMORY:
*         ...
*         break;
*       case ERR_OVERFLOW:
*         ...
*         break;
*     }
*     array.err = ERR_NONE;
*   }
*
* @author Madison Lynch
* @see    src/dynamicarray.c
* @see    README.md
*/
#ifndef _DYNAMICARRAY_H
#define _DYNAMICARRAY_H

#include <stddef.h>

/**
* Members:
*   size_t size: number of items in the dynamicarray
*   size_t *get: the "array" storing items
*   enum err: error codes supplied by supporting functions for debugging
*/
typedef struct {
    size_t size;
    size_t *get;
    enum {
        ERR_NONE,    /* 0: No errors                                              */
        ERR_INDEX,   /* 1: Invalid index provided                                 */
        ERR_MEMORY,  /* 2: Memory allocation failed                               */
        ERR_OVERFLOW /* 3: Addition of another entry would cause integer overflow */
    } err;
} dynamicarray;

/**
* Initializes and returns a new dynamicarray.
*
* @return new dynamicarray
*/
dynamicarray dynamicarray_init(void);

/**
* Allocates memory to dynamicarray and appends item.
* Ex:
*   dynamicarray_add(&array, 13);
*   dynamicarray_add(&array, "thirteen");
*
* @param  array reference to dynamicarray to modify
* @param  item  item to append (variadic for type-independence)
*/
void dynamicarray_add(dynamicarray *array, ...);

/**
* Allocates memory to dynamicarray, shifts entries as needed, and adds item at
* specified index.
* Ex:
*   dynamicarray_add(&array, "thirteen");
*   dynamicarray_insert(&array, 0, 13);
*
* @param  array reference to dynamicarray to modify
* @param  index index where item will be stored
* @param  item  item to add (variadic for type-independence)
*/
void dynamicarray_insert(dynamicarray *array, const size_t index, ...);

/**
* Removes item from dynamicarray by overwriting it, then reallocates memory.
* Ex:
*   dynamicarray_add(&array, 13);            // array.get[0] == 13
*   dynamicarray_add(&array, "thirteen");    // array.get[1] == "thirteen"
*   dynamicarray_remove(&array, 0);          // array.get[0] == "thirteen"
*
* @param  array reference to dynamicarray to modify
* @param  index index of item to remove
* @return       item to be removed from dynamicarray; returns 0 if err occurs
*/
size_t dynamicarray_remove(dynamicarray *array, const size_t index);

/**
* Reinitializes dynamicarray.
*
* @param  array reference to dynamicarray to modify
*/
void dynamicarray_clear(dynamicarray *array);

#endif