# dynamicarray
Dynamicarray is a C library that implements type-independent, dynamically sized arrays. Entries are stored as `size_t` (32-bit: 4 bytes, 64-bit: 8 bytes), accommodating for all standard types and pointers. **Remember to cast when accessing items!!**

## Notice
This project is licensed under the MIT License. See [LICENSE](LICENSE) for copyright and license details.

## Compilation
If you've installed the library to your system as a shared library (see installation section below), you can include the header file in your source project as you would any other library.
```c
#include <dynamicarray.h>
```
Then compile your project with the `-ldynamicarray` flag. 
**Example:**
```shell
gcc main.c -ldynamicarray
```

If you're compiling locally with your project, include the source file like so:
```c
#include "path/to/dynamicarray.c"
```

## Installation
This package is available in the [AUR](https://aur.archlinux.org) (Arch User Repository). So if you're on Arch GNU/Linux (or a distribution based on it), you can install it via your AUR helper.

Alternatively, you can clone this repository and build from source with [make](https://www.gnu.org/software/make/).  
  
**Install to system:** (as root)
```shell
make install
```
**Uninstall from system:** (as root)
```shell
make uninstall
```

**Generate library object file:**
```shell
make
```
**Generate demo executable:**
```shell
make demo
```
**Remove extra files (demo executable/library object file):**
```shell
make clean
```

## Usage
Since entries are stored as `size_t`, they need to be properly casted to the desired type when accessing (no need to do this when storing).

### Struct Members
**size_t size**: Number of items in the dynamicarray  
**size_t\* get**: the array of items  
**enum err:** error codes supplied by supporting functions for error handling/debugging  

### Supporting functions
**Notes:**
- "..." refers to variadic arguments. This is where you'll be passing in the items to store
- For more specific documentation (Javadoc), refer to [include/dynamicarray.h](include/dynamicarray.h)
  
dynamicarray dynamicarray_init(void)  
void dynamicarray_add(dynamicarray \*, ...)  
void dynamicarray_insert(dynamicarray \*, const size_t, ...)  
size_t dynamicarray_remove(dynamicarray \*, const size_t)  
void dynamicarray_clear(dynamicarray \*)  

**Example:**
```c
dynamicarray array = dynamicarray_init();

dynamicarray_add(&array, 13);
dynamicarray_add(&array, "thirteen");

printf("%ld\n", array.size); // Output = "2"

dynamicarray_insert(&array, 1, 0xDEADBEEF);
printf("%ld\n", array.size); // Output = "3"
printf("%d\n", (int)array.get[0]); // Output = "13"
printf("%x\n", (int)array.get[1]); // Output = "deadbeef"
printf("%s\n", (char*)array.get[2]); // Output = "thirteen"

int removed = (int)dynamicarray_remove(&array, 1);
printf("%ld\n", array.size); // Output = "2"
printf("%d\n", (int)array.get[0]); // Output = "13"
printf("%s\n", (char*)array.get[1]); // Output = "thirteen"
printf("%x\n", removed); // Output = "deadbeef"

dynamicarray_clear(&array);
printf("%ld\n", array.size); // Output = "0"
```

### Error Handling
The supporting functions may identify issues, and return early. To ensure safety, you can check the **err** struct member, and handle it accordingly.

**Errors:**
- **0 (ERR_NONE):** No errors
- **1 (ERR_INDEX):** Invalid index provided
- **2 (ERR_MEMORY):** Memory allocation failed in `realloc()`
- **3 (ERR_OVERFLOW):** Addition of another entry would cause an integer overflow

**Example:**
```c
dynamicarray_insert(&array, 5, 0xDEADBEEF); // Assume index 5 is invalid
if(array.err) {
	switch(array.err) {
		case ERR_INDEX:
			// This is the code that will execute
			break;
		case ERR_MEMORY:
			// This would execute if memory reallocation failed
			break;
		case ERR_OVERFLOW:
			/* This would execute if the addition of another entry would cause
			*  cause the "size" member to overflow.
			*/
			break;
	}
	array.err = ERR_NONE; // Reset "err" member
}
```