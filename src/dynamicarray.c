/**
* Name: dynamicarray
* Written by Madison Lynch <madi@mxdi.xyz>
* Source: https://gitlab.com/deadgirl/dynamicarray
* License: MIT (See LICENSE file for copyright and license details)
*
* Overview:
* Dynamicarray is a shared library that implements type-independent dynamically
* sized arrays. Entries are stored as "size_t" variables
* (32-bit: 4 bytes, 64-64: 8 bytes), accomodating for all basic types and
* pointers. Make sure to cast when accessing items.
*
* Unabridged documentation found in README.md file.
*
* @author Madison Lynch
* @see    include/dynamicarray.h
* @see    README.md
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#define DYNAMICARRAY_SIZE sizeof(size_t)

/**
* Members:
*   size_t size: number of items in the dynamicarray
*   size_t *get: the "array" storing items
*   enum err: error codes supplied by supporting functions for debugging
*/
typedef struct {
    size_t size;
    size_t *get;
    enum {
        ERR_NONE,    /* 0: No errors                                              */
        ERR_INDEX,   /* 1: Invalid index provided                                 */
        ERR_MEMORY,  /* 2: Memory allocation failed                               */
        ERR_OVERFLOW /* 3: Addition of another entry would cause integer overflow */
    } err;
} dynamicarray;


/**
* Resizes dynamicarray to accomodate for new entries. For use in
* dynamicarray_add() and dynamicarray_insert().
*
* @param  array reference to dynamicarray to modify
* @return       success state (EXIT_SUCCES | EXIT_FAILURE)
*/
static int
dynamicarray_realloc(dynamicarray *array) {
    if(array->size + 1 == 0) {
        array->err = ERR_OVERFLOW;
        return EXIT_FAILURE;
    }

    size_t *new_ptr = (size_t *) realloc(
        array->get, 
        ++array->size * DYNAMICARRAY_SIZE
    );

    if(!new_ptr) {
        array->err = ERR_MEMORY;
        return EXIT_FAILURE;
    } else
        array->get = new_ptr;

    (void) memset(&array->get[array->size-1], 0, DYNAMICARRAY_SIZE);
    return EXIT_SUCCESS;
}

/**
* Initializes and returns a new dynamicarray.
*
* @return new dynamicarray
*/
dynamicarray
dynamicarray_init(void) {
    return (dynamicarray) {
        0,       // size
        NULL,    // get
        ERR_NONE // err
    };
}

/**
* Allocates memory to dynamicarray and appends item.
*
* @param  array reference to dynamicarray to modify
* @param  item  item to append (variadic for type-independence)
*/
void
dynamicarray_add(dynamicarray *array, ...) {
    if(!array) {
        fprintf(stderr, "dynamicarray: NULL pointer passed.\n");
        return;
    }

    if(dynamicarray_realloc(array) == EXIT_FAILURE)
        return;

    va_list arg_ptr;
    va_start(arg_ptr, array);
    size_t item = va_arg(arg_ptr, size_t);
    va_end(arg_ptr);

    array->get[array->size-1] = item;
}

/**
* Allocates memory to dynamicarray, shifts entries as needed, and adds item at
* specified index.
*
* @param  array reference to dynamicarray to modify
* @param  index index where item will be stored
* @param  item  item to add (variadic for type-independence)
*/
void
dynamicarray_insert(dynamicarray *array, const size_t index, ...) {
    if(!array) {
        fprintf(stderr, "dynamicarray: NULL pointer passed.\n");
        return;
    }

    if(index >= array->size) {
        array->err = ERR_INDEX;
        return;
    }

    if(dynamicarray_realloc(array) == EXIT_FAILURE)
        return;

    va_list arg_ptr;
    va_start(arg_ptr, index);
    size_t item = va_arg(arg_ptr, size_t);
    va_end(arg_ptr);

    for(size_t i=array->size-1; i>index; i--)
        array->get[i] = array->get[i-1];
    (void) memset(&array->get[index], 0, DYNAMICARRAY_SIZE);
    array->get[index] = item;
}

/**
* Removes item from dynamicarray by overwriting it, then reallocates memory.
*
* @param  array reference to dynamicarray to modify
* @param  index index of item to remove
* @return       item to be removed from dynamicarray; returns 0 if err occurs
*/
size_t
dynamicarray_remove(dynamicarray *array, const size_t index) {
    if(!array) {
        fprintf(stderr, "dynamicarray: NULL pointer passed.\n");
        return 0;
    }

    if(index >= array->size) {
        array->err = ERR_INDEX;
        return 0;
    }

    size_t item = array->get[index];
    for(size_t i=index; i<array->size-1; i++)
        array->get[i] = array->get[i+1];

    size_t *new_ptr = (size_t *) realloc(
        array->get,
        --array->size * DYNAMICARRAY_SIZE
    );

    if(!new_ptr && array->size) {
        array->err = ERR_MEMORY;
        return 0;
    } else
        array->get = new_ptr;

    return item;
}

/**
* Reinitializes dynamicarray.
*
* @param  array reference to dynamicarray to modify
* @see    dynamicarray_init()
*/
void
dynamicarray_clear(dynamicarray *array) {
    if(!array) {
        fprintf(stderr, "dynamicarray: NULL pointer passed.\n");
        return;
    }

    free(array->get);
    *array = dynamicarray_init();
}