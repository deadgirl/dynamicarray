CC      = gcc
CFLAGS  = -Wall -shared
LDFLAGS = -fPIC

all:
	$(CC) $(CFLAGS) $(LDFLAGS) -o libdynamicarray.so src/dynamicarray.c

install: all
	mkdir -p /usr/lib
	cp -f libdynamicarray.so /usr/lib

	mkdir -p /usr/include
	cp -f include/dynamicarray.h /usr/include

uninstall:
	rm -f /usr/lib/libdynamicarray.so /usr/include/dynamicarray.h

demo:
	$(CC) -o demo demo.c

clean:
	rm -f demo libdynamicarray.so